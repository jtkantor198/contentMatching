'''
from keras.layers.core import *
from keras import backend as K

def call_f(inp, method, input_data):
  f = K.function([inp], [method])
  return f([input_data])[0]

def print_out(layer, input_data, train=True):
  if hasattr(layer, 'previous'):
    print call_f(layer.previous.input,
        layer.get_output(train=train), input_data)
  else:
    print call_f(layer.input, layer.get_output(train=train), input_data)
'''
import tensorflow as tf
import keras.layers.core.Masking as Masking
sess = tf.Session()

class CustomMasking(Masking):
  def get_output_mask(self, train=False):
    X = self.get_input(train)
    return K.any(K.ones_like(X) * (1. -
      K.equal(K.minimum(X, self.mask_value),
        self.mask_value)), axis=-1)

  def get_output(self, train=False):
    X = self.get_input(train)
    return X * K.any((1. - K.equal(
      K.minimum(X, self.mask_value),
        self.mask_value)), axis=-1, keepdims=True)

#print_out(Masking(mask_value=1), [[[1, 1, 0], [1, 1, 1]]])
mask = Masking(mask_value=1)
out = mask.call([[[1,1,0],[1,1,1]]])
sess = tf.Session()
print out.eval(session=sess)
