from newspaper import Article
from bs4 import BeautifulSoup
from requests import get
import json

def generateDatabase():
    data = []
    sections =  ["w","n","b","tc","e","s","snc","m"]
    url = "https://news.google.com/news/section?cf=all&pz=1&ned=us&topic="
    f = open("Database.txt",'w')
    print "sad"
    f.write(u"[")
    for section in sections:
        print section
        soup = BeautifulSoup(get(url+section).text, "html.parser")
        articleUrls = map(lambda title: title.a.attrs['href'], soup.findAll(attrs={'class':'esc-lead-article-title'}))
        for articleUrl in articleUrls:
            article = Article(articleUrl)
            article.download()
            try:
                article.parse()
            except:
                print "Parse error on "+articleUrl
                pass
                continue
            output = {'title': article.title,'text': article.text, 'date': unicode(article.publish_date), 'section': section}
            if ((articleUrl == articleUrls[-1]) and (section == sections[-1])):
                f.write((json.dumps(output)+u"\n").encode("UTF-8"))
            else:
                f.write((json.dumps(output)+u",\n").encode("UTF-8"))
    f.write(u"]")
    f.close()
    return "Done"
