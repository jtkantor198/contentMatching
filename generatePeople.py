from random import random
import json

def generatePeople():
    people = []
    for personId in range(1,51):
        interests = []
        for i in range(0,8):
            interests.push(random.randint(0, 1))
        people.push({'id': personId, 'interests': interests})
    f = open("People.txt",'w')
    f.write(json.dumps(people).encode("UTF-8"))
